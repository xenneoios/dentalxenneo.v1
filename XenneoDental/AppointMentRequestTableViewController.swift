//
//  AppointMentRequestTableViewController.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

class AppointMentRequestTableViewController: UITableViewController {

   
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let indicatorButton = self.view.viewWithTag(4)
        indicatorButton?.backgroundColor = APP_CONSTANTS.APP_THEME_COLOR
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "";
        title = "Appointment Request"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
}
