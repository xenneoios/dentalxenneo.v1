//
//  SelectDateViewController.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

//MARK: - FSCalendar Data Source/Delegate

extension SelectDateViewController :FSCalendarDataSource, FSCalendarDelegate {
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.selectedPrimaryDate = date
        
        self.showDate(date: date)
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
    }
}


class SelectDateViewController: UITableViewController {

    @IBOutlet weak var calendarContainerView: UIView!
    @IBOutlet weak var primaryDate: UILabel!
    @IBOutlet weak var primaryDay: UILabel!
    @IBOutlet weak var finalDate: UILabel!
    @IBOutlet weak var finalDateDesc: UILabel!
    
    @IBOutlet weak var alternateDate1: UILabel!
    @IBOutlet weak var alternateDate2: UILabel!
    
    var selectedPrimaryDate : Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let indicatorButton = self.view.viewWithTag(3)
        indicatorButton?.backgroundColor = APP_CONSTANTS.APP_THEME_COLOR
        // Do any additional setup after loading the view.
        
        calendarContainerView.layer.cornerRadius = 10.0
        calendarContainerView.layer.borderWidth = 1.0
        calendarContainerView.layer.borderColor = APP_CONSTANTS.APP_THEME_CGCOLOR
        calendarContainerView.layer.masksToBounds = true
        
        self.showDate(date: NSDate() as Date)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "";
        title = "Select Three Dates"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showDate(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE"//"EE" to get short style
        var dayInWeek = dateFormatter.string(from: date)//"Sunday"
        
        self.primaryDay.text = dayInWeek;
        
        dateFormatter.dateFormat  = "dd"//"EE" to get short style
        dayInWeek = dateFormatter.string(from: date)//"Sunday"
        
        self.primaryDate.text = dayInWeek
        self.finalDate.text = dayInWeek
        
        dateFormatter.dateFormat  = "EEEE MMMM YYYY"//"EE" to get short style
        dayInWeek = dateFormatter.string(from: date)//"Sunday"
        self.finalDateDesc.text = dayInWeek
        
    }

}
