//
//  DentalLocationsViewController.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit


extension DentalLocationsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.hostpitalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :  DentalLocationTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "DentalLocationTableViewCell") as? DentalLocationTableViewCell)!
        
        var hospitalDict = self.hostpitalArray[indexPath.row];
        
        cell.hospitalName.text = hospitalDict["name"] as? String
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController : SelectDateViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

class DentalLocationsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var hospitalTable: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var consultationType: UILabel!
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var locationTable: UITableView!
    
    var docName : String = ""
    var reasonForVisit : String = ""
    var notesStr : String = ""
    var hostpitalArray : [ [String : Any]] = [[:]]
    override func viewDidLoad() {
        super.viewDidLoad()


        let indicatorButton = self.view.viewWithTag(2)
        indicatorButton?.backgroundColor = APP_CONSTANTS.APP_THEME_COLOR
        // Do any additional setup after loading the view.
        searchBar.layer.borderColor = APP_CONSTANTS.APP_THEME_CGCOLOR
        searchBar.layer.borderWidth = 1.0
        searchBar.layer.cornerRadius = 10.0

        self.doctorName.text = self.docName
        self.consultationType.text = self.reasonForVisit
        self.notes.text = notesStr
        
        self.hospitalTable.tableFooterView = UIView.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "";
        title = "Our Locations"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
