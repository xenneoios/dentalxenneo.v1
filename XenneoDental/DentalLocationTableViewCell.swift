//
//  DentalLocationTableViewCell.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 11/03/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

class DentalLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var hospitalName: UILabel!
    @IBOutlet weak var hospitalImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
