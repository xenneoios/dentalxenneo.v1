//
//  ConfirmationTableViewController.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

class ConfirmationTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let indicatorButton = self.view.viewWithTag(5)
        indicatorButton?.backgroundColor = APP_CONSTANTS.APP_THEME_COLOR
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "";
        title = "Confirmation"
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
