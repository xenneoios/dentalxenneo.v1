//
//  BookAppintmentHome.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

extension BookAppintmentHome : CustomPopOverViewDelegate {
    
    func selectedIndex(index: NSInteger) {
        
        if viewTag == 1 {
            let titleStr = self.doctorArray[index]
            self.chhoseDocButton.setTitle(titleStr, for: UIControlState.normal)
        }
        else if viewTag == 2 {
            self.chhoseDocButton.setTitle("", for: UIControlState.normal)
            let titleStr = self.reasonForConsult[index]
            self.chooseReasonButton.setTitle(titleStr, for: UIControlState.normal)
            self.fetchDoctor(department: "\(index + 1)")
        }
    }
}


class BookAppintmentHome: UITableViewController  {
    
    @IBOutlet weak var chhoseDocButton: UIButton!
    @IBOutlet weak var chooseReasonButton: UIButton!

    var viewTag = 0

    @IBOutlet weak var reasonView: UIView!
    @IBOutlet weak var chooseDoc: UIView!
    @IBOutlet weak var dentalOffice: UIView!
    var pageIndex = 1;
    @IBOutlet weak var notes: UITextView!
    
    var doctorArray = [""]
    var staffForDepartment = [[String : Any]]()
    var reasonForConsult = ["Cardiology","Psychology"]
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationItem.title = "Appointment"
        
        pageControlButton()
        // Do any additional setup after loading the view, typically from a nib.
        notes.layer.borderColor = APP_CONSTANTS.APP_THEME_CGCOLOR
        notes.layer.borderWidth = 1.0
        notes.layer.cornerRadius = 10.0
        
        chooseDoc.layer.cornerRadius = 10.0
        chooseDoc.clipsToBounds = true
        reasonView.layer.cornerRadius = 10.0
        reasonView.clipsToBounds = true
        dentalOffice.layer.cornerRadius = 10.0
        dentalOffice.clipsToBounds = true

        self.chooseReasonButton.setTitle(" " + reasonForConsult[0], for: UIControlState.normal)

        self.chhoseDocButton.setTitle(" " + doctorArray[0], for: UIControlState.normal)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        fetchDepartment()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Appointment"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    @IBAction func chooseDoctor(_ sender: Any) {
        viewTag = 1;
    //let docButton : UIButton = sender as! UIButton
        
        if doctorArray.count != 0 {
            let viewController : CustomPopOverViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomPopOverViewController") as! CustomPopOverViewController
            viewController.dataArray = doctorArray
            viewController.titleStr = "Doctors"
            viewController.delegate = self
            viewController.modalPresentationStyle = .overCurrentContext
            self.view.window?.rootViewController?.present(viewController, animated: true)
        }
        else {
            let alert = UIAlertController.init(title: "", message:"No doctors for this department", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: {
                
            })
        }
    }
    
    @IBAction func chooseDentalOffice(_ sender: Any) {
        
        
        let doctorName : String = (self.chhoseDocButton.titleLabel?.text)!
        let doctorIndex = self.doctorArray.index(of: doctorName)
        
        if let doctorIndex = doctorIndex {
            let hospitalArray = (self.staffForDepartment[doctorIndex])["hospitalArray"]
            
            let viewController : DentalLocationsViewController = self.storyboard?.instantiateViewController(withIdentifier: "DentalLocationsViewController") as! DentalLocationsViewController
            viewController.docName = doctorName
            viewController.notesStr = self.notes.text
            viewController.reasonForVisit = (self.chooseReasonButton.titleLabel?.text)!
            viewController.hostpitalArray = hospitalArray as! [[String : Any]]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            DispatchQueue.main.async(execute: {
                let alert = UIAlertController.init(title: "", message:"Please select a doctor.", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: {
                    
                })
            })
        }
        
        
    }
    
    
    @IBAction func chooseReason(_ sender: Any) {
        viewTag = 2;
        //let docButton : UIButton = sender as! UIButton
        let viewController : CustomPopOverViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomPopOverViewController") as! CustomPopOverViewController
        viewController.dataArray = reasonForConsult
        viewController.titleStr = "Reason"
        viewController.delegate = self
        viewController.modalPresentationStyle = .overCurrentContext
        self.view.window?.rootViewController?.present(viewController, animated: true)
        
    }
    
    func pageControlButton()  {
        for index in 1..<5 {
            let indicatorButton = self.view.viewWithTag(index)
            if index == pageIndex {
                indicatorButton?.backgroundColor = APP_CONSTANTS.APP_THEME_COLOR
            }
            else {
                indicatorButton?.backgroundColor = UIColor.white
            }
        }
    }
    
    func fetchDepartment () {
        
        SVProgressHUD.show()

        let json: [String: String] = ["objName": "LKDepartment"]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let loginURLStr : String = APP_CONSTANTS.getDepartment
        let url = URL(string: loginURLStr)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "No Data")

            DispatchQueue.main.async(execute: {
                SVProgressHUD.dismiss()
            })
            
            guard let data = data, error == nil else {
                let errorStr = (error?.localizedDescription ?? "There is some problem connecting to server.")
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController.init(title: "", message:errorStr, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: {
                        
                    })
                })
                return
            }
            self.fetchDoctor(department: "\(1)")
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            let myjson = responseJSON as? [String:Any]
            if let responseJSON = responseJSON {
                print(responseJSON)
                

            }
        }
        task.resume()
    }
    
    func fetchDoctor (department : String) {
        
        SVProgressHUD.show()
        
        let json: [String: String] = ["objName": "EOStaff","lkDepartment" : department]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let loginURLStr : String = APP_CONSTANTS.getStaffByDepartment
        let url = URL(string: loginURLStr)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "No Data")

            DispatchQueue.main.async(execute: {
                SVProgressHUD.dismiss()
            })
            
            guard let data = data, error == nil else {
                let errorStr = (error?.localizedDescription ?? "There is some problem connecting to server.")
                DispatchQueue.main.async(execute: {
                    let alert = UIAlertController.init(title: "", message:errorStr, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: {
                        
                    })
                })
                return
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let responseJSON = responseJSON as? [[String : Any]] {
                print(responseJSON)
                
                self.staffForDepartment = responseJSON
                
                self.doctorArray.removeAll()
                for staff in self.staffForDepartment {
                    let staffName : String = staff["fullName"] as! String
                    self.doctorArray.append(staffName)
                }
                
            }
        }
        task.resume()
    }
}

