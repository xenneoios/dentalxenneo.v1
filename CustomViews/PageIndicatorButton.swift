//
//  PageIndicatorButton.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

class PageIndicatorButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //TODO: Code for our button
        layer.borderWidth = 1.0
        layer.borderColor = APP_CONSTANTS.APP_THEME_CGCOLOR
        clipsToBounds = true
        layer.cornerRadius = self.frame.size.height / 2
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
       
    }
    

}
