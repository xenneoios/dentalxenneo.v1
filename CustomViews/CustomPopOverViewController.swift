//
//  CustomPopOverViewController.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 09/03/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

protocol CustomPopOverViewDelegate : class {
    func selectedIndex(index : NSInteger);
}

class CustomPopOverViewController: UIViewController {
    var dataArray : Array<Any> = []
    weak var delegate: CustomPopOverViewDelegate?
    
    @IBOutlet weak var customtable: UITableView!
    @IBOutlet weak var titleLabel: UINavigationItem!
    var titleStr = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.title = titleStr
        self.customtable.layer.borderWidth =  1
        self.customtable.layer.borderColor = APP_CONSTANTS.APP_THEME_CGCOLOR
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
}



extension CustomPopOverViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "basic")
        
        tableCell?.textLabel?.text = self.dataArray[indexPath.row] as? String
        
        if indexPath.row % 2 == 0 {
            tableCell?.backgroundColor = APP_CONSTANTS.APP_THEME_COLOR
        }
        else {
            tableCell?.backgroundColor = UIColor.white
        }
        
        
        return tableCell!
    }
    
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        self.dismiss(animated: true) {
            self.self.delegate?.selectedIndex(index: indexPath.row)
        }
    }
}
