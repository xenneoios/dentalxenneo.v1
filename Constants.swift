//
//  Constants.swift
//  XenneoDental
//
//  Created by Abhishek Kumar on 22/02/18.
//  Copyright © 2018 Kent. All rights reserved.
//

import UIKit

struct APP_CONSTANTS {

    static let APP_THEME_COLOR:UIColor = UIColor.init(red: 93.0/255, green: 153.0/255, blue: 214.0/255, alpha: 1)
    static let APP_THEME_CGCOLOR = UIColor.init(red: 93.0/255, green: 153.0/255, blue: 214.0/255, alpha: 1).cgColor

private struct Domains {
    static let ROOT_URL = "http://139.59.65.76:8080"
}

private  struct Routes {
    static let Api = "/XenneoDental/rest"
}
    
private  static let Domain = Domains.ROOT_URL
private  static let Route = Routes.Api
private  static let BaseURL = Domain + Route

static var login: String {
    return BaseURL  + "/auth/loginUser"
}
    
static var getDepartment: String {
    return BaseURL  + "/ajax/getObject"
}
    
static var getStaffByDepartment: String {
    return BaseURL  + "/schedule/getStaffByDepartment"
}

}
